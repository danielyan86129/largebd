% shapename = 'twotri'; 
% input_folder = ['D:\Yajie\code\geom-opt\shapes\multi_step\' shapename];
% initshapename = [shapename '_init'];
% restMfile = fullfile(input_folder,shapename);
% initMfile = fullfile(input_folder,initshapename);
% hdlfile = fullfile(input_folder,[shapename '_hdls.dmat']);
% restM = importMesh(restMfile,'2d','obj');
% initM = importMesh(initMfile,'2d','obj');
% hdls = importHandles(hdlfile);
% K = 9e6;
% % K = 1.13865e+06
% max_iter = 100;
% n_step = 3;
% autoK = false;

% shapename = '100639_cc000000'; 
% input_folder = ['D:\Yajie\code\geom-opt\shapes\multi_step\' shapename];
% initshapename = [shapename '_init_0000'];
% restMfile = fullfile(input_folder,shapename);
% initMfile = fullfile(input_folder,initshapename);
% hdlfile = fullfile(input_folder,[shapename '_hdls.dmat']);
% restM = importMesh(restMfile,'3d','tetgen');
% initM = importMesh(initMfile,'3d','tetgen');
% hdls = importHandles(hdlfile);
% % sigma = max(max(restM.V)-min(restM.V)) * 0.001;
% % restM.V = restM.V + sigma*randn(length(restM.V),size(restM.V,2));
% % initM.V = initM.V + sigma*randn(length(restM.V),size(restM.V,2));

% shapename = '101582_cc000000'; 
% input_folder = ['D:\Yajie\code\geom-opt\shapes\multi_step\' shapename];
% initshapename = [shapename '_init_0002'];
% restMfile = fullfile(input_folder,shapename);
% initMfile = fullfile(input_folder,initshapename);
% hdlfile = fullfile(input_folder,[shapename '_hdls.dmat']);
% restM = importMesh(restMfile,'3d','tetgen');
% initM = importMesh(initMfile,'3d','tetgen');
% hdls = importHandles(hdlfile);

shapename = '100639_cc000001'; 
testtype = 'twist';
input_folder = 'D:\Yajie\code\geom-opt\shapes\smalltest-morehdls\';
initname = [shapename '_init_0000'];
hdlname = [shapename '_hdls.dmat'];
restMfile = fullfile(input_folder,'shapes',shapename,shapename);
initMfile = fullfile(input_folder,shapename,testtype,initname);
hdlfile = fullfile(input_folder,shapename,testtype,hdlname);
restM = importMesh(restMfile,'3d','tetgen');
initM = importMesh(initMfile,'3d','tetgen');
hdls = importHandles(hdlfile);
metafile = fullfile(input_folder,shapename,testtype,[initname '.testmeta']);
dfm_info = TestUtil.mkDeformInfo(metafile, hdls);
% sigma = max(max(restM.V)-min(restM.V)) * 0.001;
% restM.V = restM.V + sigma*randn(length(restM.V),size(restM.V,2));
% initM.V = initM.V + sigma*randn(length(restM.V),size(restM.V,2));
K = 5.5;
% K = 1.13865e+06
max_iter = 1000;
n_step = 100;
autoK = false;
stop_at_inj = false;

% shapename = '100639_cc000001'; 
% input_folder = ['D:\Yajie\code\geom-opt\shapes\multi_step\' shapename];
% initshapename = [shapename '_init_0000'];
% restMfile = fullfile(input_folder,shapename);
% initMfile = fullfile(input_folder,initshapename);
% hdlfile = fullfile(input_folder,[shapename '_hdls.dmat']);
% restM = importMesh(restMfile,'3d','tetgen');
% initM = importMesh(initMfile,'3d','tetgen');
% hdls = importHandles(hdlfile);
% sigma = max(max(restM.V)-min(restM.V)) * 0.0001;
% % restM.V = restM.V + sigma*randn(length(restM.V),size(restM.V,2));
% % initM.V = initM.V + sigma*randn(length(restM.V),size(restM.V,2));
% K = 5.5;
% % K = 1.13865e+06
% max_iter = 5000;
% n_step = 100;
% autoK = false;

% warning('off','MATLAB:nearlySingularMatrix');
out_folder = fullfile(input_folder, ['res_' datestr(now,'mmddyyyyHHMMSS')]);
img_folder = fullfile(out_folder, 'ms_img');
mkdir(img_folder);
fg_base = shapename;
% K = 5.5;
[resultM,n_suc,stats] = multistepOptimize(restM, initM, hdls, dfm_info,...
    K, autoK, max_iter, n_step, false, stop_at_inj, fg_base, img_folder);
% fprintf('num of success steps = %d\n', n_suc);
% whereas if you optimize in one step
% optimizeInstance(restM,initM,hdls,K,max_iter,true,false);
warning('on','MATLAB:nearlySingularMatrix');