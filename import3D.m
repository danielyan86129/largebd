function [ rest_m, init_m, hdls ] = import3D( basename, format )
%IMPORT3D import 3D rest/init mesh files and handle file
%   Returns: rest mesh, init mesh, handle indices

rest_m = importMesh( basename, '3d', 'dmat' );
init_m = importMesh( strjoin({basename, '_init'}, ''), '3d', 'dmat' );
hdls = importHandles( strjoin({basename,'_hdls.dmat'}, '') );

end

