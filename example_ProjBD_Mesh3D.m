%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code implementing the paper "Large-Scale Bounded Distortion Mappings".
% Disclaimer: The code is provided as-is for academic use only and without any guarantees. 
%             Please contact the author to report any bugs.
% Written by Shahar Kovalsky (http://www.wisdom.weizmann.ac.il/~shaharko/)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% init
rng(1)
clear
addpath('mex');
  

%% parameters
n = 15; % problem size
sigma = .3; % noise level (for initial map)
K = 1.5; % conformal distortion bound
lb = -1; % lower bound on SVs (-1 = disabled)
ub = -1; % upper bound on SVs (-1 = disabled)
iter_max = 10; % maximal number of BD projection iterations
tol_err = 1e-15; % tolerance for stopping BD projection iterations
use_weighted_metric = false; % use a weighted metric?


%% generate problem
% generate regular mesh
[x, y, z] = ndgrid(1:n,1:n,1:n);
V = [x(:), y(:), z(:)];
F = delaunay(V);

% some constants
dim = size(F,2)-1;
n_vert = size(V,1);
n_tri = size(F,1);

%% randome initial map & fix centroid constraint
%[x0, eq_lhs, eq_rhs] = GenConstraints('fix-centroid', sigma, [], V, n_vert, dim, n);

%% corner-moving constraint
displacement = zeros(8, 3);
displacement(5,:) = displacement(5,:) + [+8,+8,-8];
displacement(8,:) = displacement(8,:) + [-4,-4,-4];
%displacement(6,:) = displacement(6,:) + [-8,0,0];
[x0, eq_lhs, eq_rhs] = GenConstraints('corners', sigma, displacement, V, n_vert, dim, n);

%% solve problem
% setup BD solver
solver_bd = SolverProjectorBD(F, V, eq_lhs, eq_rhs, K, lb, ub, x0, SolverProjectorModeEnum.Tangent, use_weighted_metric);

% plot initial map
figure;
solver_bd.visualize();
title('Initial Map');
cameratoolbar;
cameratoolbar('SetCoordSys','none');

% run solver
solver_bd.solve(iter_max, tol_err, false); % solve BD projection

% plot output map
figure;
solver_bd.visualize();
title('Output Map');
cameratoolbar;
cameratoolbar('SetCoordSys','none');


%% functions specifying constraints
function [x0, eq_lhs, eq_rhs] = GenConstraints(type, sigma, displacement, V, n_vert, dim, n)
    switch type
        case 'fix-centroid'
            x0 = V + sigma*randn(n_vert,dim);
            % setup linear constraints (fix centroid)
            eq_lhs = kron(eye(dim),ones(1,n_vert))/n_vert;
            eq_rhs = eq_lhs*colStack(x0); % rhs is the centroid of x0
        case 'corners'
            % 8 corners of the mesh
            c = [1,n,n*n-n+1,n*n];
            c = [c,c+(n*n*(n-1))];
            c_vts = V(c,:); disp(c_vts);
            c_vts = c_vts + displacement;
            A = zeros(numel(c),n_vert);
            for i = 1:numel(c)
                A(i,c(i)) = 1;
            end
            eq_lhs = kron(eye(dim), A);
            eq_rhs = colStack(c_vts);
            %initial map
            x0 = V;
            x0(c,:) = c_vts;
    end
end