function createSurfaceMeshBatch( filelist, out_dir )
%createSurfaceMeshBatch creates a triangle mesh for each given file path
%representing a tet mesh. 
%If out_dir == '', the triangle mesh is written in
%the same folder as the tet mesh; otherwise, the triangle mesh will be
%output to the specified folder
for i = 1:numel(filelist)
    % read the tet
    [path, name, ~] = fileparts(filelist{i});
    in_base = fullfile(path, name);
    tet = importMesh(in_base, '3d', 'tetgen');
    % write tet's surface as a .off mesh
    if strcmp(out_dir, '')
        out_file = fullfile(path, [name '.off']);
    else
        out_file = fullfile(out_dir, [name '.off']);
    end
    fprintf('Writing... %s\n', out_file);
    writeOFF(out_file, tet.V, tet.B);
end

end

