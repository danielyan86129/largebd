%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code implementing the paper "Large-Scale Bounded Distortion Mappings".
% Disclaimer: The code is provided as-is for academic use only and without any guarantees. 
%             Please contact the author to report any bugs.
% Written by Shahar Kovalsky (http://www.wisdom.weizmann.ac.il/~shaharko/)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef SolverProjectorBD < SolverProjector
    
    properties
        V; % Yajie: rest shape vertices
        F;
        K;
        lb;
        ub;
        dim;
        distortions;
        flips;
        minsv; % before projection min/max svs
        maxsv;
        pMinsv; % Yajie: after projection min/max svs (before opt)
        pMaxsv;
        oMinsv; % Yajie: output min/max svs (after opt)
        oMaxsv;
        alliter_minsv; % Yajie: record min/max svs for all iterations
        alliter_maxsv; 
        use_weighted_metric;
        % stats to return
        stats = SolverStats;
        % states to save/recover
        save_stats;
    end
    
    methods
        function obj = SolverProjectorBD(F, V, eqLHS, eqRHS, K, lb, ub, x0, mode, use_weighted_metric)
            % setup BD solver
            obj.report(1,'-----------------------------------------------------------------\n');
            obj.report(1,'Constructing SolverProjectorBD (vertices %d,  elements %d)\n', size(V,1), size(F,1));
            t_start = tic;
            obj.V = V;
            obj.F = F;
            obj.eqLHS = eqLHS;
            obj.eqRHS = eqRHS;
            obj.x0 = x0;
            obj.mode = mode;
            obj.use_weighted_metric = use_weighted_metric;
            obj.K = K;
            obj.lb = lb;
            obj.ub = ub;
            obj.dim = size(F,2)-1;
            [obj.T,areas] = computeMeshTranformationCoeffsMex(F, V); % compute mapping of vertices to differentials (T)
            weights = kron(sqrt(areas),ones(obj.dim^2,1));
            if use_weighted_metric,
                obj.W = sparse(1:length(weights),1:length(weights),weights);
            else
                obj.W = 1;
            end
            obj.initSolver(); % initialize
            obj.report(1,'SolverProjectorBD is ready (%.3g secs)\n', toc(t_start));
            obj.report(1,'-----------------------------------------------------------------\n\n');
        end
        
        function projectD_(obj)
            % project onto BD
            [obj.pTx, obj.distortions, obj.flips, ...
                obj.minsv, obj.maxsv, obj.pMinsv, obj.pMaxsv] = ...
                projectBDMex(obj.Tx, obj.dim, obj.K, obj.lb, obj.ub);
        end
        
        % Yajie: save/restore current states of the solver
        function save(obj)
            obj.save_stats.tanNormal = obj.tanNormal;
            obj.save_stats.x = obj.x;
            obj.save_stats.Tx = obj.Tx;
            obj.save_stats.pTx = obj.pTx;
            obj.save_stats.distortions = obj.distortions;
            obj.save_stats.flips = obj.flips;
        end
        function restore(obj)
            obj.tanNormal = obj.save_stats.tanNormal;
            obj.x = obj.save_stats.x;
            obj.Tx = obj.save_stats.Tx;
            obj.pTx = obj.save_stats.pTx;
            obj.distortions = obj.save_stats.distortions;
            obj.flips = obj.save_stats.flips;
        end
        % Yajie: update the flip tag for each simplex (obj.flips)
        function updateFlipTags(obj)
            Tx_cpy = obj.T*obj.x;
            Tx_cpy = reshape(Tx_cpy, obj.dim*obj.dim, [])';
            for i = 1:size(Tx_cpy,1)
                C = reshape(Tx_cpy(i,:), obj.dim, obj.dim);
                det_C = det(C);
                obj.flips(i) = det_C < -1e-100;
            end
        end
        
        function fig_res = solve(obj, iter_max, tol_err, ...
                data_file, save_jacobian, save_metric, ...
                save_fig, inj_stop)
            obj.report(1,'-----------------------------------------------------------------\n');
            obj.report(1,'BD PROJECTION (K=%g):\n', obj.K);
            obj.report(1,'-----------------------------------------------------------------\n');
            obj.report(1,'initial max dist %g,  flips %d  (infeasible %d)\n', ...
                max(obj.distortions), nnz(obj.flips), nnz((obj.distortions>obj.K)|obj.flips));
            %obj.report(1,'(min sv %g,  max sv %d)\n', min(abs(obj.minsv)), max(obj.maxsv));
            obj.report(1,'-----------------------------------------------------------------\n');
            
            opt_data_fid = -1;
            if save_jacobian || save_metric
                opt_data_fid = fopen(data_file,'wt');
            end
            if save_jacobian
                opt_data_fid = fopen(data_file,'wt');
                % dim, 2*max-iter, size of each line (maps at each iteration)
                fprintf(opt_data_fid,'%d %f %d %d\n',obj.dim,obj.K,2*iter_max+1,numel(obj.Tx));
            end
            obj.t_iter_total = 0;
            obj.stats.tol_reached = false;
            % Yajie: prepare for making figures
            fig_res = 0;
            if save_fig
                fig_res = zeros(2*(iter_max+1),1); %alloc space for all fig handles
                [dist_colors, flip_color] = getColors;
                fig_sample_rate = max(floor(iter_max / 200), 1); %figuring at this rate
                fig_res(1) = obj.return_curframe(dist_colors, flip_color); %save initial map
            end
            
            % how many messages across all iters you want to see?
            totalmsg = 1; %10;
            print_rate = max(floor(iter_max / totalmsg), 1);
            for iter = 1:iter_max
                % Yajie: optionally save Tx before projection
                if save_jacobian
                    fprintf(opt_data_fid,'%f ',obj.Tx);fprintf(opt_data_fid,'\n','');
                end
                obj.iterate();
                if save_jacobian
                    fprintf(opt_data_fid,'%f ',obj.pTx);fprintf(opt_data_fid,'\n','');
                end
                if save_metric
                    fprintf(opt_data_fid, obj.get_metric_str());
                end
                obj.t_iter_total = obj.t_iter_total+obj.t_iter;
                %obj.updateFlipTags(); % Yajie: reflect the very now flip status
                % Yajie: optionally save result (e.g. figure)
                if save_fig && mod(iter, fig_sample_rate) == 0
                    fig_res(2*(iter-1)+1+1) = obj.return_curSimplCompl(dist_colors);
                    fig_res(2*(iter-1)+1+2) = obj.return_curframe(dist_colors, flip_color);
                end
                % Yajie: optionally save Tx before & after projection
                % print stats and terminate if needed
                err = mse(obj.tanNormal);
                if mod(iter, print_rate) == 0 || iter == iter_max || err < tol_err ...
                        || (inj_stop == true && nnz(obj.flips)==0)
                    obj.report(1,'iter %d -- err: %g    dist: %g    flips: %g   time: %g sec (pD:%d%%, pL:%d%%)\n',iter,err,max(obj.distortions),nnz(obj.flips),obj.t_iter,round(100*obj.t_projectD/obj.t_iter), round(100*obj.t_projectLinear/obj.t_iter));    
                end
                % save svs stats of this iter
                %obj.alliter_minsv = [obj.alliter_minsv, obj.minsv, obj.pMinsv];
                %obj.alliter_maxsv = [obj.alliter_maxsv, obj.maxsv, obj.pMaxsv];
                
                % record if bound achieved
                obj.stats.n_infeasible = nnz(obj.distortions > obj.K | obj.flips);
                if (obj.stats.n_infeasible == 0)
                    obj.stats.bd_iter = iter;
                end
                % record first inj iter when injectivity achieved
                obj.stats.n_flip = nnz(obj.flips);
                if (obj.stats.n_flip == 0)
                    if obj.stats.inj_iter < 0
                        obj.stats.inj_iter = iter;
                    end
                end
                % terminate?
                obj.stats.converged = obj.isConverged(inj_stop, tol_err, err);
                if obj.stats.converged
                    if inj_stop
                        obj.report(1,'injectivity reached --> stopping...\n');
                    else
                        obj.stats.tol_reached = true;
                        obj.report(1,'err<tol_err or feasible obtained --> stopping...\n');
                    end
                    break;
                end
            end
            obj.report(1,'-----------------------------------------------------------------\n');
            obj.report(1,'final max dist %g,  flips %d  (infeasible %d)\n', ...
                max(obj.distortions), nnz(obj.flips), nnz((obj.distortions>obj.K)|obj.flips));
            obj.report(1,'-----------------------------------------------------------------\n\n');
            obj.stats.total_iter = iter;
            obj.stats.total_t = obj.stats.total_t + obj.t_iter_total;
            obj.stats.iter_t = obj.t_iter;
            % save the output map
            if save_jacobian
                fprintf(opt_data_fid,'%f ',obj.Tx);fprintf(opt_data_fid,'\n','');
            end
            % close file
            if save_jacobian || save_metric
                fclose(opt_data_fid);
            end
            %finally, shrink size of fig_res 
            if save_fig && numel(fig_res) > 2*iter + 1
                fig_res(2*iter+2:end) = [];
            end
        end
        
        function done = isConverged(obj, inj_stop, tol_err, err)
            if inj_stop == true
                if obj.stats.n_flips == 0
                    done = true;
                else
                    done = false;
                end
            elseif err<tol_err || obj.stats.n_infeasible == 0
                done = true;
            else
                done = false;
            end
        end
        
        function visualize(obj)
            [dist_colors,flip_color] = getColors;
            switch size(obj.F,2)
                case 3 % triangular mesh
                    sides = [1 2 3];
                case 4 % tetraherdral mesh
                    sides = [1 2 3; 1 2 4; 1 3 4; 2 3 4];
                otherwise
                    error('Invalid problem type')
            end
            dist = obj.distortions;
            dist(dist < obj.K) = 0.0;
            for kk = 1:size(sides,1)
                patch('vertices',obj.y,'faces',obj.F(:,sides(kk,:)),'FaceVertexCData',dist,'facecolor','flat','EdgeAlpha',0.1,'facealpha',0.6);
                patch('vertices',obj.y,'faces',obj.F(:,sides(kk,:)),'FaceVertexCData',zeros(size(obj.F,1),1),'facecolor','flat','EdgeAlpha',0.1,'facealpha',0.6);
                patch('vertices',obj.y,'faces',obj.F(logical(obj.flips),sides(kk,:)),'FaceVertexCData',obj.distortions,'facecolor','none','edgecolor',flip_color);
            end
            cameratoolbar('SetMode', 'orbit');
            cameratoolbar('SetCoordSys', 'none');
            axis equal;
            axis on;
            xlabel('X');
            ylabel('Y');
            zlabel('Z');
            colormap(dist_colors);
            colorbar;
            caxis([1 1.5*obj.K]);
        end
        
        %% return a bunch of metric (in char)
        function msg = get_metric_str(obj)
            msg = sprintf('%s: %.8e, %s: %.8e, %s: %.8e, %s: %.8e\n',...
                'err', mse(obj.tanNormal),...
                'max-flip-distortion', max(obj.distortions(obj.flips>0)),...
                'max-pos-distortion', max(obj.distortions),...
                'max-per-v-d', max(vecnorm(obj.y-obj.x0, 2)));
        end
        
        %% return the handle to a figure depicting current iteration of obj
        function fg = return_curframe(obj, dist_colors, flip_color)
            fg = figure('Visible','Off');
            switch size(obj.F,2)
                case 3 % triangular mesh
                    sides = [1 2 3];
                case 4 % tetraherdral mesh
                    sides = [1 2 3; 1 2 4; 1 3 4; 2 3 4];
                otherwise
                    error('Invalid problem type')
            end
            for kk = 1:size(sides,1)
                %patch('vertices',obj.y,'faces',obj.F(:,sides(kk,:)),'FaceVertexCData',obj.distortions,'facecolor','flat','EdgeAlpha',0.1,'facealpha',0.6);
                patch('vertices',obj.y,'faces',obj.F(:,sides(kk,:)),'FaceVertexCData',zeros(size(obj.F,1),1),'facecolor','flat','EdgeAlpha',0.1,'facealpha',0.6);
                patch('vertices',obj.y,'faces',obj.F(logical(obj.flips),sides(kk,:)),'FaceVertexCData',obj.distortions,'facecolor','none','edgecolor',flip_color);
            end
%             text( obj.y(:,1), obj.y(:,2), cellstr(num2str( (1:size(obj.y,1))' )) );
            axis equal;
            axis on;
            xlabel('X');
            ylabel('Y');
            zlabel('Z');
            colormap(dist_colors);
            colorbar;
            caxis([1 1.5*obj.K]);
        end
        
        %% return the figure of simplex complex using current pTx (proj map)
        function fg = return_curSimplCompl(obj, dist_colors)
            nfaces = size(obj.F,1);
            dsimp = obj.dim+1;
            max_pert = 0.2*max(max(obj.x0) - min(obj.x0));
            % get per-simplex mapping ready
            pTx = reshape(obj.pTx, obj.dim*obj.dim, []); 
            % store new vertices for all simplices
            newFvts = zeros( nfaces, obj.dim*dsimp );
            for si = 1:nfaces
%                 fprintf("stats for simplex %d (after proj):\n",si);
                curFmap = reshape(pTx(:,si),obj.dim,obj.dim);
                curFvts = obj.V(obj.F(si,:),:)';
%                 disp("curFmap:");
%                 disp(curFmap);
%                 disp("curFvts:");
%                 disp(curFvts);
                %slightly perturb location of mapped face to separate them
                %visually
                pert = rand(obj.dim,1);
                pert = pert / norm(pert) * max_pert;
                newFvts(si,:) = colStack(curFmap * curFvts)'; 
%                 disp("newFvts:");
%                 disp(newFvts(si,:));
%                 fprintf("newFvts-ids for simplex %d:\n",si);
%                 disp(obj.F(si,:));
            end
            newFvts = reshape(newFvts', obj.dim, [])';
            newFvts_ids = colStack(obj.F');
%             disp('newFvts:');
%             disp(newFvts);
%             disp('newFvts-ids:');
%             disp(newFvts_ids);
            
            fg = figure('Visible', 'Off');
            patch('XData',reshape(newFvts(:,1),dsimp,[]), ...
                'YData',reshape(newFvts(:,2),dsimp,[]), ...
                'FaceVertexCData',zeros(size(obj.F,1),1), ...
                'facecolor','flat','EdgeAlpha',0.1,'facealpha',0.6);
%             text( newFvts(:,1), newFvts(:,2), cellstr(num2str( newFvts_ids )) );
            axis equal;
            axis on;
            xlabel('X');
            ylabel('Y');
            zlabel('Z');
            colormap(dist_colors);
            colorbar;
            caxis([1 1.5*obj.K]);
        end
    end
    
end

