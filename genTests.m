function genTests( testgenfile )
%GENTESTS generates all tests specified in the given file
fid = fopen(testgenfile, 'rt');
% skip comments
ln = strtrim(fgetl(fid));
while ln(1) == '#'
    ln = strtrim(fgetl(fid));
end

% parse header
config = containers.Map();
format = ln;
dim = strtrim(fgetl(fid));
testtypes = split(strtrim(fgetl(fid)));
%display(testtypes);
ln = strtrim(fgetl(fid));
hdl_ratio = sscanf(ln, '%f %f %f');
config('hdl_ratio') = num2cell(hdl_ratio');

% read all shapes' names
shapes2test = textscan(fid,'%s','delimiter','\n');
shapes2test = shapes2test{:};
%display(shapes{1});
% close file
fclose(fid);

% for each shape, generate all test types
[testgenpath,~,~] = fileparts(testgenfile);
allshapefolder = fullfile(testgenpath,'shapes');
gen_all_tests(allshapefolder, shapes2test, testtypes, format, dim, testgenpath, config);
end

function gen_all_tests(allshapefolder, shapes2test, testtyps, format, dim, testgenpath, config)
for i=1:numel(shapes2test)
    shapename = strtrim(shapes2test{i});
    shapepath = fullfile(allshapefolder,shapename,shapename);
    outfolder = fullfile(testgenpath,shapename);
    try
        restm = importMesh(shapepath, dim, format);
    catch import_e
        fprintf('error occured in importMesh:\n')
        warning(import_e.message);
    end
    hdl_ratio = config('hdl_ratio');
    try
        TestUtil.genTest(restm, shapepath, outfolder, hdl_ratio{:}, false);
    catch me
        fprintf('something wrong during test-gen for shape %s\n',shapename);
        warning(me.message);
    end
end
end

