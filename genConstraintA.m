
%% GENCONSTRAINTA generates the constraint equality Ax = b
%   x0 is initial mesh vertices
function [lh, rh] = genConstraintA( hdls, x0, d, n )
try
    rh = colStack( x0( hdls, : ) );
    A = sparse( numel( hdls ), n );
    inds = sub2ind( size(A), 1:size(hdls), hdls' );
    A( inds ) = 1;
%     A = sparse(A);
    lh = kron( eye(d), A );
    assert(sprank(lh) == min(size(lh)),'handle constraint not full rank!');
catch me
    fprintf('error in genConstraintA with hdls size %d, vts size %d\n',...
        length(hdls), n);
    warning(me.message);
end

end
