function [ restm, initm, hdls ] = import2D( basename )
%IMPORT2D import 2D rest/init mesh files and handle file
%   Returns: rest mesh, init mesh, handle indices

restm = importMesh( basename, '2d' );
initm = importMesh( strjoin({basename,'_init'}, ''), '2d' );
hdls = importHandles( strjoin({basename,'_hdls.dmat'}, '') );

end