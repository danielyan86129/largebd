%% make a video out of given figures
function saveFigs(figs, basename)
    fprintf('saving (%d) figures to %s...\n', numel(figs), basename );
    if exist(basename, 'dir') ~= 7
        mkdir(basename);
    end
    
    % put all figures in same view (link axes)
    all_axes = zeros(numel(figs));
    for i = 1:numel(figs)
        all_axes(i) = findall(figs(i), 'type', 'axes');
    end
%     linkaxes(all_axes);
    
    for i = 1:numel(figs)
        fprintf('saving fig %d\n',i);
        print( figs(i), fullfile( basename, strcat('fig_',int2str(i)) ), '-dpng' );
    end
    %making video
    video_obj = VideoWriter( fullfile(basename, 'out') );
    video_obj.FrameRate = 3;
    open(video_obj);
    for i = 1:numel(figs)
        writeVideo(video_obj, getframe(figs(i)));
    end
    close(video_obj);
    for i = figs
        close(i);
    end
end