function createTetsInFolder( folder )
%createTetsInFolder creates a tet mesh for each shape in given folder
%   input:
%       - folder: containing shapes in files with supported format
formats = {'.ply','.off'};
files = dir(folder);
for i=1:numel(files)
    f = files(i);
    % skip unsupported files
    [~,meshname,ext] = fileparts(f.name);
    if ~ismember(lower(ext), formats)
        continue;
    end
    % read in the mesh
    mesh_file = fullfile(folder,f.name);
    fprintf('reading mesh from %s\n', mesh_file);
    [V,F] = read_mesh(mesh_file);
    % tetrahedralize it
    [tV,tT,tB] = my_tetgen(V,F);
    tet_m.V = tV;
    tet_m.B = tB;
    tet_m.F = tT;
    % write tet mesh to its own folder
    out_folder = fullfile(folder, meshname);
    if ~exist(out_folder, 'dir')
        mkdir(out_folder);
        out_base = fullfile(out_folder, meshname);
        fprintf('writing tet to %s\n', out_folder);
        writeTETGEN(tet_m, out_base);
    end
end
end

