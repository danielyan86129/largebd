function [suc, result_m] = runTest( dinfo, format, restfile, initfile, hdlfile, Ks, iter_max, showfig )
%RUNTEST on given rest/init mesh and handles, over a bunch of test
%parameters
%   Detailed explanation goes here

suc = false;
rest_m = importMesh(restfile, dinfo, format);
init_m = importMesh(initfile, dinfo, format);
hdls = importHandles(hdlfile);
stop_at_inj = false;
for k = Ks
    [result_m, suc] = optimizeInstance(rest_m, init_m, hdls, k, iter_max, showfig, stop_at_inj);
    if suc == true
        disp("Succeeded on k="+num2str(k));
        break;
    end
end
if ~suc
    disp("Failed on all ks");
end
end

