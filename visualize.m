%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Visualize a mesh with faces F and vertices V
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function visualize(V, F, K)
    [dist_colors,flip_color] = getColors;
    F_scals = ones(size(F,1),1);
    switch size(F,2)
        case 3 % triangular mesh
            sides = [1 2 3];
        case 4 % tetraherdral mesh
            sides = [1 2 3; 1 2 4; 1 3 4; 2 3 4];
        otherwise
            error('Invalid problem type')
    end
    %     patch('Faces',F,'Vertices',V);
    for kk = 1:size(sides,1)
        %patch('Faces',F,'Vertices',V);
        patch('vertices',V,'faces',F(:,sides(kk,:)),...
            'FaceVertexCData',F_scals,...
            'facecolor','flat','EdgeAlpha',0.1,'facealpha',0.6);
        %patch('vertices',obj.y,'faces',obj.F(logical(obj.flips),sides(kk,:)),'FaceVertexCData',obj.distortions,'facecolor','none','edgecolor',flip_color);
    end
    cameratoolbar('SetMode', 'orbit');
    cameratoolbar('SetCoordSys', 'none');
    axis equal;
    axis off;
    colormap(dist_colors);
    colorbar;
    caxis([1 1.5*K]);
end