function max_dist = maxDistortion( restM, targetM )
%maxDistortion return max distortion given the rest shape and a target
%shape
%   Detailed explanation goes here
K = 1.5; % any value > 1 would do
solver_tmp = SolverProjectorBD(restM.F, restM.V, [], [], K, -1, -1, ...
    targetM.V, SolverProjectorModeEnum.Tangent, false);
max_dist = max(1, abs(max(solver_tmp.distortions))); % automatically determine K >= 1

end

