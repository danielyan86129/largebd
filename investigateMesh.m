orig_temp_dir = getenv('temp');
cur_temp_dir = 'tmp';
if ~exist(cur_temp_dir )
    mkdir(cur_temp_dir );
end
setenv('temp',cur_temp_dir );

shapename = '100639_cc000001'; 
input_folder = ['D:\Yajie\code\geom-opt\shapes\smalltest-morehdls'];
initshapename = [shapename '_init_0000'];
testtype = 'twist';
restMfile = fullfile(input_folder,'shapes',shapename,shapename);
initMfile = fullfile(input_folder,shapename,testtype,initshapename);
hdlfile = fullfile(input_folder,shapename,testtype,[shapename '_hdls.dmat']);
restM = importMesh(restMfile,'3d','tetgen');
initM = importMesh(initMfile,'3d','tetgen');
hdls = importHandles(hdlfile);

di_angle = rad2deg(dihedral_angles(restM.V, restM.F));
max_di_angle = max(max(di_angle));
min_di_angle = min(min(di_angle));
fprintf('di-hedral angle of rest-mesh: [%f, %f]\n', min_di_angle, max_di_angle);

% %% now remesh rest-mesh boundary
% % get boundary mesh
% bndry_restM = getBoundaryMesh(restM);
% [tetV,tetT,~] = my_tetgen(bndry_restM.V, bndry_restM.F);
% remeshed_restM.V = tetV; remeshed_restM.F = tetT;
% new_bndry_restM = getBoundaryMesh(remeshed_restM);
% %% check remeshed boundary
% di_angle = dihedral_angles(remeshed_restM.V, remeshed_restM.F);
% max_di_angle = max(max(di_angle));
% min_di_angle = min(min(di_angle));
% fprintf('di-hedral angle of remeshed rest-mesh: [%f, %f]\n', ...
%     rad2deg(min_di_angle), rad2deg(max_di_angle));

%% plot boundary mesh
% figure;
% plot_mesh(bndry_restM.V,bndry_restM.F);
% title('orig boundary');
% figure;
% plot_mesh(new_bndry_restM.V,new_bndry_restM.F);
% title('remeshed boundary');
figure;
hold on;
axis equal; 
axis vis3d;
graycolor = [0.5,0.5,0.5];
% 
% drawMesh(remeshed_restM.V,remeshed_restM.F,'FaceColor',graycolor);
% draw mesh with handles
drawPoint3d(restM.V(hdls,:),'Marker','o','MarkerFaceColor','r');
drawMesh(restM.V,restM.F,'FaceColor','none','FaceAlpha',0.02,'EdgeAlpha',0.1);
drawPoint3d(initM.V(hdls,:),'Marker','o','MarkerFaceColor','r');
drawMesh(initM.V,initM.F,'FaceColor','none','FaceAlpha',0.02,'EdgeAlpha',0.1);

hold off;

setenv('temp',orig_temp_dir);