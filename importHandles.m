function hdls = importHandles( filename )
%IMPORTHANDLES reads in an array of handle indices
%   Note: this function expects the handles in the file are 0-based. 
hdls = readDMAT( filename );
hdls = hdls + 1;

end

