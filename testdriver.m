format = 'tetgen';
% shapedir = 'D:\Yajie\code\geom-opt\shapes\lbd_test-test\shapes';
% testgenfile = 'D:\Yajie\code\geom-opt\shapes\lbd_test-test\test-gen-2.txt';
% testrunfile = 'D:\Yajie\code\geom-opt\shapes\lbd_test-test\test-run-2.txt';
% shapedir = 'D:\Yajie\code\geom-opt\shapes\tets-3929';
% testgenfile = 'D:\Yajie\code\geom-opt\shapes\lbd_test-tets-3929\test-gen.txt';
% testrunfile = 'D:\Yajie\code\geom-opt\shapes\lbd_test-tets-3929\test-run-100.txt';
% logfile = 'D:\Yajie\code\geom-opt\shapes\lbd_test-tets-3929\test-run-100-log.txt';
shapedir = 'D:\Yajie\code\geom-opt\shapes\tets-3929';
testgenfile = 'D:\Yajie\code\geom-opt\shapes\lbd_test-1\test-gen-1.txt';
testrunfile = 'D:\Yajie\code\geom-opt\shapes\lbd_test-1\test-run-1.txt';
logfile = 'D:\Yajie\code\geom-opt\shapes\lbd_test-1\test-run-1-log.txt';

try
    % make the test-gen file
    % makeTestGenFile(format, shapedir, testgenfile);
    % generate tests instances guided by the test-gen file
%     genTests(testgenfile);
    % % make a test-to-run file
    % makeTestRunFile(format, shapedir, testrunfile);
    % run those tests specified in the test-to-run file
    warning('off','MATLAB:nearlySingularMatrix');
    if exist(logfile, 'file')
        diary off;
        delete(logfile);
    end
    diary(logfile);
    runTests(testrunfile);
    diary off;
    warning('on','MATLAB:nearlySingularMatrix');
catch me
    diary off;
    fclose('all');
    warning(me.message);
end
