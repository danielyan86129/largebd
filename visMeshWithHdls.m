function visMeshWithHdls( m_or_f, hdl_or_f, varargin )
%VISMESHWITHHDLS given the file name for a mesh and handles, visualize
%the mesh with the handles on it
if isa(m_or_f, 'char')
    m = importMesh(m_or_f,'3d','tetgen');
else
    m = m_or_f;
end
if isa(hdl_or_f, 'char')
    hdls = importHandles(hdl_or_f);
else
    hdls = hdl_or_f;
end

figure;
camlight;
cameratoolbar('SetMode', 'orbit');
cameratoolbar('SetCoordSys', 'none');
axis vis3d;
hold on;
graycolor = [0.7 0.7 0.7];
drawPoint3d(m.V(hdls,:),'MarkerEdgeColor','r');

if numel(varargin) > 1
    if strcmp(varargin{2},'tet')
        drawMesh(m.V, m.F, 'EdgeColor','none','FaceColor',graycolor,...
            'FaceAlpha',.15,'FaceLighting','flat');
    else
        drawMesh(m.V, m.B, 'EdgeColor','none','FaceColor',graycolor,...
            'FaceLighting','gouraud');
    end
else
    drawMesh(m.V, m.B, 'EdgeColor','none','FaceColor',graycolor,...
        'FaceLighting','gouraud');
end
if numel(varargin) > 0
    title(varargin{1});
end
hold off;
end

