function writeTETGEN( tetm, base_out )
%writeTETGEN write given tet mesh to tetgen format files (.node/face/ele)
node_name = [base_out '.node'];
ele_name = [base_out '.ele'];
face_name = [base_out '.face'];
writeNODE(node_name, tetm.V, 'MinIndex', 0);
writeELE(ele_name, tetm.F - 1, 'MinIndex', 0);
my_writeFACE(face_name, tetm.B - 1, 'MinIndex', 0);

end

