%% runTests runs all tests listed in the given file,
%  output: writes results to another file
function runTests(testfile)
[rootfolder,testfilename,~] = fileparts(testfile);
allshapefolder = fullfile(rootfolder, 'shapes');

% fid = fopen(testfile, 'r');
[config, allshapes] = TestUtil.importRunConfig(testfile);
stop_at_inj = strcmp(config.goal, 'inj-only');
K_range = config.k;

% output folder
out_folder = fullfile(rootfolder, ['res_' datestr(now,'mmddyyyyHHMMSS')]);
mkdir(out_folder);
copyfile(testfile, out_folder);
imgres_folderpath = fullfile(out_folder, 'img');
mkdir(imgres_folderpath);
% log file
log_file = fullfile(out_folder, 'log.txt');
diary(log_file);
% save results in this cell array
result = {};
% cell array will be written to this file
resultfile = fullfile(out_folder, [char(testfilename) '_res.csv']);

try
    n_inj_test_suc = 0;
    n_bd_test_suc = 0;
    n_shape_inj_suc = 0;
    n_shape_bd_suc = 0;
    result = addresult(result, {'rest shape','type','init shape','injectivity',...
        '1st working K','actual K','n flip','inj iter','n infeasible','bd iter',...
        'T','T-iter','img'});
    cell2csv(resultfile, result, 'w', ',');
    result = {};
    for si = 1:numel(allshapes)
        % get shape name,
        % which might contain a different upper bound for k
        shapename = allshapes{si};
        k_ub = K_range(3);
        res_tpl = strsplit(shapename);
        if length(res_tpl) == 2
            shapename = res_tpl{1};
            k_ub = str2double(res_tpl{2});
        end
        Ks = K_range(1):K_range(2):min(K_range(3), k_ub);
        
        % read shape's rest mesh
        restfile = fullfile(allshapefolder, shapename, shapename);
        rest_m = importMesh( restfile, config.dim, config.format );
        
        % for each test
        no_test_failed = true;
        for ti = 1:numel(config.tests)
            testname = config.tests{ti};
            display(testname);
            testfolder = fullfile(rootfolder, shapename, testname);
            % read the handles
            hdlfile = fullfile( testfolder, [shapename '_hdls.dmat'] );
            hdls = importHandles(hdlfile);
            % analyze how many init meshes are there
            allfiles = dir(testfolder);
            allnames = {allfiles.name}';
            pat_init = [shapename '_init'];
            initfiles = allnames(startsWith(allnames, pat_init));
            % extract names of init meshes
            initfiles_split = split(initfiles, '.');
            initnames = unique(initfiles_split(:,1));
            
            % now run optimization on the tuple <rest, init, hdls>
            for i = 1:numel(initnames)
                k = Ks(1);
                try
                    initname = initnames{i};
                    fprintf("\n"+initname+"\n");
                    init_m = importMesh( fullfile(testfolder, initname), ...
                        config.dim, config.format );
                    suc = false;
                    stats = SolverStats;
                    fg_name = '';
                    
                    % multistep solver related
                    ms_img_folder = fullfile(out_folder,...
                        'ms_img',shapename,testname,initname);
                    if ~exist(ms_img_folder, 'dir')
                        mkdir(ms_img_folder);
                    end
                    cur_metafile = fullfile(testfolder, [initname '.testmeta']);
                    dfm_info = TestUtil.mkDeformInfo(cur_metafile, hdls);
                    % for both solvers
                    res_m_folder = fullfile(out_folder,...
                        'res_mesh',shapename,testname,initname);
                    if ~exist(res_m_folder, 'dir')
                        mkdir(res_m_folder);
                    end
                    
                    % try a bunch of K
                    for k = Ks
                        % report status
                        fprintf("testing shape: %d, %s, %s, %f\n",si,testname,initname,k);
                        fprintf("%d shapes left\n",numel(allshapes)-si);
                        % run test with this K
                        if strcmp(config.solver, 'one-step')
                            [result_m, suc, stats] = optimizeInstance(...
                                rest_m, init_m, hdls,...
                                k, config.maxiter, config.tol_err,...
                                false, stop_at_inj,...
                                '');
                        elseif strcmp(config.solver, 'multi-step')
                            [result_m, suc, stats] = multistepOptimize(...
                                rest_m, init_m, hdls, dfm_info, ...
                                config.r_multi, config.ada_step, ...
                                k, false, config.maxiter, config.tol_err, config.nstep, ...
                                false, stop_at_inj, initname, ms_img_folder);
                        else
                            fprintf('Error: "%s", unrecognized solver!\n', config.solver);
                        end
                        % save figure anyway
                        [~,imgfoldername,~] = fileparts(imgres_folderpath);
                        fg_name = fullfile(imgfoldername,[initname '-' testname '-' num2str(k) '.png']);
                        fg_path = fullfile(imgres_folderpath,[initname '-' testname '-' num2str(k) '.png']);
                        fg = figure('visible','off');
                        visualize(result_m.V,result_m.F,k);
                        saveas(fg,fg_path);
                        close(fg);
                        % save result mesh (as boundary and tet)
                        res_m_base = fullfile(res_m_folder, ['res_' num2str(k)]);
                        writeOFF([res_m_base '.off'],result_m.V,result_m.B);
                        writeTETGEN(result_m, res_m_base);
                        % stop if succeeded for this k
                        if suc
                            disp("Succeeded on k = "+num2str(k));
                            result = addresult(result, ...
                                {shapename, testname, initname, ...
                                "success", k, stats.final_dist,...
                                stats.n_flip, stats.inj_iter, ...
                                stats.n_infeasible, stats.bd_iter, ...
                                stats.total_t, stats.iter_t, ...
                                ['=HYPERLINK("' fg_name '")']});
                            cell2csv(resultfile, result, 'a', ',');
                            result = {};
                            break;
                        end
                    end
                    no_test_failed = no_test_failed & suc;
                    if suc
                        n_inj_test_suc = n_inj_test_suc+1;
                        if stats.n_infeasible == 0
                            n_bd_test_suc = n_bd_test_suc+1;
                        end
                    else
                        disp("Failed on all ks");
                        result = addresult(result, ...
                            {shapename, testname, initname,... 
                            "failure", k, stats.final_dist,...
                            stats.n_flip, stats.inj_iter, ...
                            stats.n_infeasible, stats.bd_iter, ...
                            stats.total_t, stats.iter_t, ...
                            ['=HYPERLINK("' fg_name '")']});
                        cell2csv(resultfile, result, 'a', ',');
                        result = {};
                    end
                catch me
                    no_test_failed = false;
                    result = addresult(result, ...
                        {shapename, testname, initname,...
                        "error", k, stats.final_dist,...
                        stats.n_flip, stats.inj_iter, ...
                        stats.n_infeasible, stats.bd_iter, ...
                        stats.total_t, stats.iter_t, ...
                        ['=HYPERLINK("' 'bad_img' '")']});
                    cell2csv(resultfile, result, 'a', ',');
                    result = {};
                    disp(['Error: cannot optimize ' initname]);
                    disp(getReport(me));
                end
                % display(result);
            end
        end
        n_shape_inj_suc = n_shape_inj_suc + no_test_failed;
        fprintf('\n--------------------\n');
        fprintf('So far tests passed for %d shapes, failed for %d shapes\n',...
            n_shape_inj_suc, si-n_shape_inj_suc);
        fprintf('--------------------\n');
    end
    cell2csv(resultfile, result, 'a', ',');
catch me
    diary off;
    fclose('all');
    warning(me.message);
end
diary off;
end

function resarray = addresult(resarray, linedata)
sz = size(resarray);
resarray{sz(1)+1, numel(linedata)} = '';
resarray(sz(1)+1, :) = linedata;
end