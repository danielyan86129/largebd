%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code implementing the paper "Large-Scale Bounded Distortion Mappings".
% Disclaimer: The code is provided as-is for academic use only and without any guarantees. 
%             Please contact the author to report any bugs.
% Written by Shahar Kovalsky (http://www.wisdom.weizmann.ac.il/~shaharko/)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% init
rng(1)
clear
addpath('mex');


%% parameters
n = 2; % problem size
sigma = .3; % noise level (for initial map)
% K = 1.5; %conformal distortion bound. 
K = 99;
%K = 1.5;
% K = 2.5;
lb = -1; % lower bound on SVs (-1 = disabled)
ub = -1; % upper bound on SVs (-1 = disabled)
iter_max = 2000; % maximal number of BD projection iterations
tol_err = 1e-50; % tolerance for stopping BD projection iterations
use_weighted_metric = false; % use a weighted metric?


%% generate problem
% generate regular mesh
[x, y] = ndgrid(1:n,1:n);
V = [x(:), y(:)];
F = delaunay(V);

% some constants
dim = size(F,2)-1;
n_vert = size(V,1);
n_tri = size(F,1);
	
% initial map & constrain centroid
%[x0, eq_lhs, eq_rhs] = GenConstraints('fix-centroid',sigma,[],V,n_vert,dim,n);

% constraint on corners: 
% move lower-right corner away from shape
%displacement = [0,0;0,0;0,0;0.3,0.3];
%type = 'corners';
% move two opposite corners toward each other by intermediate amount
%displacement = [0.2,0.2;0,0;0,0;-0.2,-0.2];
%type = 'corners';
% ...      opposite ...                       by large amount
%displacement = [0.7,0.7;0,0;0,0;-0.7,-0.7];
%type = 'corners';
% move one corner by intermediate amount toward center
%displacement = [0,0;0,0;0,0;-0.6,-0.6];
%type = 'corners';
% move one corner by large amount ...
%displacement = [0,0;0,0;0,0;-0.7,-0.7];
%type = 'corners';
% only constrain one corner ...
%displacement= [-2.8,-2.];
displacement= [-999.5,-999.5];
type = '1-corner';

[x0, eq_lhs, eq_rhs] = GenConstraints(type,sigma,displacement,V,n_vert,dim,n);


%% solve problem
% setup BD solver
solver_tmp = SolverProjectorBD(F, V, eq_lhs, eq_rhs, K, lb, ub, x0, SolverProjectorModeEnum.Tangent, use_weighted_metric);
K = max(1, abs(max(solver_tmp.distortions))); % automatically determine K >= 1
fprintf('auto K = %f', K);
clear solver_tmp;
solver_bd = SolverProjectorBD(F, V, eq_lhs, eq_rhs, K, lb, ub, x0, SolverProjectorModeEnum.Tangent, use_weighted_metric);

% plot rest shape
figure;
visualize(V, F, K);
title('Rest shape');
hA(1) = gca;

% plot initial map
figure;
solver_bd.visualize();
title('Initial Map');
hA(2) = gca;

% run solver
record_fig = false;
save_opt_data = true;
data_file = ['opt_data_' num2str(n_vert) '_' num2str(n_tri) '_' num2str(K) '.txt'];
fig_res = solver_bd.solve(iter_max, tol_err, save_opt_data, data_file, ...
    record_fig, false); % solve BD projection
% post-process figures
video_name = strjoin({type,char(strjoin(string(displacement),',')),num2str(K)},'_');
video_base = fullfile(fileparts(mfilename('fullpath')), 'videos', '2d', video_name);
if record_fig
    saveFigs(fig_res, video_base);
end

% plot output map
figure;
solver_bd.visualize();
title('Output Map');
hA(3) = gca;

linkaxes(hA);

% plot SVs evolution
% figure;
% plotSVs(solver_bd.alliter_minsv, solver_bd.alliter_maxsv, 1, 'b--x');
% figure;
% plotSVs(solver_bd.alliter_minsv, solver_bd.alliter_maxsv, 2, 'r--o');

%% functions specifying constraints
function [x0, eq_lhs, eq_rhs] = GenConstraints(type, sigma, displacement, V, n_vert, dim, n)
    switch type
        case 'fix-centroid'
            x0 = V + sigma*randn(n_vert,dim);
            % setup linear constraints (fix centroid)
            eq_lhs = kron(eye(dim),ones(1,n_vert))/n_vert;
            eq_rhs = eq_lhs*colStack(x0); % rhs is the centroid of x0
        case 'corners'
            % corners of the mesh
            c = [1,n,n*n-n+1,n*n];
            c_vts = V(c,:);
            c_vts = c_vts + displacement*max(max(V)-min(V));
            A = zeros(numel(c),n_vert);
            for i = 1:numel(c)
                A(i,c(i)) = 1;
            end
            eq_lhs = kron(eye(dim), A);
            eq_rhs = colStack(c_vts);
            %initial map
            x0 = V;
            x0(c,:) = c_vts;
        case '1-corner'
            % corners of the mesh
            c = [n*n];
            c_vts = V(c,:);
            c_vts = c_vts + displacement*max(max(V)-min(V));
            A = zeros(numel(c),n_vert);
            for i = 1:numel(c)
                A(i,c(i)) = 1;
            end
            eq_lhs = kron(eye(dim), A);
            eq_rhs = colStack(c_vts);
            %initial map
            x0 = V;
            x0(c,:) = c_vts;
    end
end