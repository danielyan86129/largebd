%%% set temporary folder
cur_temp_dir = 'tmp';
if strcmp(getenv('temp'),cur_temp_dir)
    orig_temp_dir = getenv('temp');
    if ~exist(cur_temp_dir)
        mkdir(cur_temp_dir);
    end
    setenv('temp',cur_temp_dir);
end

%%% read list of meshes to remesh
shapedir = 'D:\Yajie\code\geom-opt\shapes\tets-3929';
meshlist_file = 'D:\Yajie\code\geom-opt\shapes\remesh_test\meshlist.txt';
[outdir,~,~] = fileparts(meshlist_file);
fid = fopen(meshlist_file, 'rt');
meshnames = textscan(fid, '%s', 'Delimiter', '\n');
meshnames = meshnames{:};
fclose(fid);

%%% for each meshname, read it from shapedir, and remesh the boundary
for i=1:numel(meshnames)
    cur_name = meshnames{i};
    meshfile_base = fullfile(shapedir, cur_name, cur_name);
    fprintf('Processing %s \n', cur_name);
    mesh = importMesh(meshfile_base, '3d', 'tetgen');
    reportMeshQuality(mesh, 'dihedral angle BEFORE remesh');
    % get boundary mesh
    bndry_m.V = mesh.V;
    bndry_m.F = mesh.B;
    % remesh
    [tetV,tetT,~] = my_tetgen(bndry_m.V, bndry_m.F);
    remeshed_m.V = tetV;
    remeshed_m.F = tetT;
    remeshed_m.B = boundary_faces(remeshed_m.F);
    reportMeshQuality(remeshed_m, 'dihedral angle AFTER remesh');
    % output remeshed
    mesh_outdir = fullfile(outdir, cur_name);
    if ~exist(mesh_outdir)
        mkdir(mesh_outdir);
    end
    base_out = fullfile(mesh_outdir, cur_name);
    writeTETGEN(remeshed_m, base_out);
%     node_name = fullfile(mesh_outdir, [cur_name '.node']);
%     ele_name = fullfile(mesh_outdir, [cur_name '.ele']);
%     face_name = fullfile(mesh_outdir, [cur_name '.face']);
%     writeNODE(node_name, remeshed_m.V, 'MinIndex', 0);
%     writeELE(ele_name, remeshed_m.F - 1, 'MinIndex', 0);
%     my_writeFACE(face_name, remeshed_m.B - 1, 'MinIndex', 0);
end

setenv('temp', orig_temp_dir);

function reportMeshQuality(m, msg)
di_angle = rad2deg(dihedral_angles(m.V, m.F));
max_di_angle = max(max(di_angle));
min_di_angle = min(min(di_angle));
fprintf('%s: [%f, %f]\n', msg, min_di_angle, max_di_angle);
end