function BM = getBoundaryMesh( tetmesh )
%getBoundaryMesh extract a boundary triangular mesh from given tet mesh
%   return a structure {V, F}, 
%   V: n-by-3, n is num of boundary vts
%   F: m-by-3, m is num of boundary faces. 
%   Note: faces index into V, not keeping old indices in tetmesh.

BF = boundary_faces(tetmesh.F);
BV_ids = unique(BF);
oldId_2_newId = zeros(size(tetmesh.V,1),1);
oldId_2_newId(BV_ids) = (1:numel(BV_ids))';
BM.F = oldId_2_newId(BF);
BM.V = tetmesh.V(BV_ids,:);

end

