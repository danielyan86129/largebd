function [ result_m, success, stats ] = optimizeInstance( ...
    rest_m, init_m, hdls, ...
    K, iter_max, tol_err, showfigs, stop_at_inj, ...
    aux_file)
%OPTIMIZEINSTANCE One-shot LBD
% Input:
%   rest_m, init_m, hdls: rest/init mesh, and handle indices
%   K: bd K
%   iter_max: maximally allowed iterations
%   tol_err: termination threshold for the normal to the tangent plane at
%   projection
%   showfigs: show figures or not
%   stop_at_inj: injective-only or achieving full bd?
%   aux_file: a file to store extra info about the optimization
%   
lb = -1; % lower bound on SVs (-1 = disabled)
ub = -1; % upper bound on SVs (-1 = disabled)
n = size(rest_m.V, 1);
d = size(rest_m.V, 2);
%tol_err = 1e-15; % BD projection tolerance
% tol_err = 1e-50;
use_weighted_metric = false; % use a weighted metric?

x0 = init_m.V;
[C_lh, C_rh] = genConstraintA( hdls, x0, d, n );

solver_bd = SolverProjectorBD( rest_m.F, rest_m.V, C_lh, C_rh, K, lb, ub, x0, ...
    SolverProjectorModeEnum.Tangent, use_weighted_metric );

if showfigs
axis equal
axis vis3d
camproj('persp');
% plot rest shape
figure;
visualize( rest_m.V, rest_m.F, K );
title('Rest shape');
hA(1) = gca;

% plot initial map
figure;
solver_bd.visualize();
title('Initial Map');
hA(2) = gca;
end

% run solver
record_fig = false;
save_jacobian = false;
save_metric = false;
data_file = aux_file;
t_solve = tic;
fig_res = solver_bd.solve( iter_max, tol_err, ...
    data_file, save_jacobian, save_metric, ...
    record_fig, stop_at_inj ); % solve BD projection
t_solve = toc(t_solve);
fprintf('Time elapsed: solve - %f sec, iterate - %f sec.\n',t_solve,solver_bd.t_iter_total);
stats = solver_bd.stats;
% post-process figures
if record_fig
    video_base = fileparts( mfilename('fullpath') );
    video_base = fullfile( video_base, 'videos', '2d' );
    saveFigs(fig_res, video_base);
end

% set results
result_m.V = solver_bd.y;
result_m.F = solver_bd.F;
result_m.B = boundary_faces(result_m.F);
if stop_at_inj
    success = stats.n_flip == 0;
else
    success = stats.n_flip == 0 && stats.tol_reached;
end
stats.final_dist = max(solver_bd.distortions);

if showfigs
% plot output map
figure;
solver_bd.visualize();
title('Output Map');
hA(3) = gca;

linkaxes(hA);
end

% plot SVs evolution
% figure;
% plotSVs(solver_bd.alliter_minsv, solver_bd.alliter_maxsv, 1, 'b--x');
% figure;
% plotSVs(solver_bd.alliter_minsv, solver_bd.alliter_maxsv, 2, 'r--o');

end

