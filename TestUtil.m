classdef TestUtil
    %TestUtil is a collection of functions used for gen-tets and
    %run-test
    
    properties
    end
    
    methods(Static)
        function [restM,initM,hdls,dfm] = importOptData(...
                folder, rest_name, init_name, test)
            rest_base = fullfile(folder,'shapes',rest_name,rest_name);
            init_base = fullfile(folder,rest_name,test,init_name);
            hdl_f = fullfile(folder,rest_name,test,[rest_name '_hdls.dmat']);
            meta_f = [init_base '.testmeta'];
            restM = importMesh(rest_base,'3d','tetgen');
            initM = importMesh(init_base,'3d','tetgen');
            hdls = importHandles(hdl_f);
            dfm = TestUtil.mkDeformInfo(meta_f,hdls);
        end
        function visOptData(folder, rest_name, init_name, test, varargin)
            [restM,initM,hdls,~] = TestUtil.importOptData(...
                folder,rest_name,init_name,test);
            if numel(varargin) > 0
                visMeshWithHdls(restM,hdls,'rest shape',varargin{1});
                visMeshWithHdls(initM,hdls,'init shape',varargin{1});
            else
                visMeshWithHdls(restM,hdls,'rest shape');
                visMeshWithHdls(initM,hdls,'init shape');
            end
        end
        function visResultMesh(...
                folder,rest_name,init_name,test,result_name,varargin)
            resM_f = fullfile(folder,rest_name,test,init_name,result_name);
            if numel(varargin) > 0
                visMeshWithHdls(resM_f,[],'result shape',varargin{1});
            else
                visMeshWithHdls(resM_f,[],'result shape');
            end
        end
        
        function [rest_base,init_base,hdl_f,meta_f] = mkFileNames(...
                folder, rest_name, init_name, test)
            rest_base = fullfile(folder,'shapes',rest_name,rest_name);
            init_base = fullfile(folder,rest_name,test,init_name);
            hdl_f = fullfile(folder,rest_name,test,[rest_name '_hdls.dmat']);
            meta_f = [init_base '.testmeta'];
        end
        
        function [config,allshapes] = importRunConfig(filepath)
            config = ini2struct(filepath, ['mesh']);
            config = config.settings;
            config.k = str2double(strsplit(config.k));
            config.maxiter = str2double(config.maxiter);
            if isfield(config, 'tol_err')
                config.tol_err = str2double(config.tol_err);
            else
                config.tol_err = 1e-15;
            end
            config.tests = split(strtrim(config.tests));
            if isfield(config, 'nstep')
                config.nstep = str2double(config.nstep);
            end
            if isfield(config, 'ada_step')
                config.ada_step = logical(str2double(config.ada_step));
            end
            if isfield(config, 'r_multi')
                config.r_multi = str2double(config.r_multi);
            end
            % then we need to parse meshnames ourselves
            fid = fopen(filepath);
            while ~feof(fid)
                ln = strtrim(fgetl(fid));
                if strcmp(ln,'[mesh]')
                    break;
                end
            end
            % following are mesh names (skip commented lines)
            allshapes = textscan(fid,'%s','delimiter','\n');
            allshapes = allshapes{1};
            allshapes = allshapes(~startsWith(allshapes,'#'));
        end
        
        function suc = genTest( m, shapepath, folder, p_h1, p_h2, p_h3, showfig )
            %Given the rest mesh, generate a set of tests and output them to files.
            %   rest mesh is a tuple <V, F, B>
            %   V is n by 3, F is m by 4, B is the boundary faces
            %   Each test is a tuple <initial mesh, handle set>
            %   Each test will be output to the folder corresponding to the testtype
            %   Currently 3 test types are supported: twist, stretch, and bend
            %   p_h1/2/3: percentage along major axis for selecting handle set 1, 2, 3
            
            [~,shapename,~] = fileparts(shapepath);
            fprintf('Generating test for %s \n', shapename);
            %% generate twist/stretch direction from PCA analysis of point cloud
            [coeff,score,~] = pca(m.V);
            assert( all(size(coeff)==[3,3])||all(size(coeff)==[2,2]), 'PCA error');
            % 1st PC is rotation / stretch direction
            d1 = coeff(:,1);
            % 2nd PC is bending axis
            d2 = coeff(:,2);
            
            %% generate three handle sets (two for twist/stretch, add. third for bending)
            % each row of score corresponds to a vertex projected onto the 3 PCs
            % a handle set corresponds to those vts that are within some interval of
            % the score along 1st PC
            b_vts_ids = unique(reshape(m.B, 1, []));
            if numel(b_vts_ids) < 3
                disp(['Not enough vts for specifying handles. Cannot generate test for ' shapepath]);
                suc = false;
                return;
            end
            b_flag_v = false(size(m.V,1),1);
            b_flag_v(b_vts_ids) = true;
            proj_pc = rescale(score(:,1)); % 1st PC
            [~, I] = sort(proj_pc); %sort based on 1st PC
            max_sz = range(score(:,1));
            h1 = find(proj_pc <= p_h1 & b_flag_v); % bottom
            if numel(h1) == 0
                h1 = I(end);
            end
            h2 = find(proj_pc >= 1.0-p_h2 & b_flag_v); % top
            if numel(h2) == 0
                h2 = I(1);
            end
            h3 = find(0.5-p_h3/2<=proj_pc & proj_pc<=0.5+p_h3/2 & b_flag_v); % middle
            if numel(h3) == 0
                h3 = I(ceil(numel(I)/2));
            end
            TestUtil.gen_twist(folder, shapepath, m, d1, h2, h1, 20, 180, 1, showfig);
            TestUtil.gen_stretch(folder, shapepath, m, d1, h2, h1, -0.5*max_sz, 1.1*max_sz, 1, showfig);
            TestUtil.gen_bend(folder, shapepath, m, d2, h1, h2, h3, 10, 60, 1, showfig);
            suc = true;
        end
        
        function gen_twist(folder, shapepath, m, ...
                d, h_top, h_bot, minA, maxA, n, show_fig)
            %% generate twisting tests
            folder = fullfile(folder, 'twist');
            % if exist(folder, 'dir')
            %     fprintf('folder "%s" exists; skipping gen test...', folder);
            %     return
            % end
            if exist(folder, 'dir')
                warning('folder "%s" exists; overwriting...', folder);
                rmdir(folder,'s');
            end
            mkdir(folder);
            h1V = m.V(h_top,:);
            h2V = m.V(h_bot,:);
            if length(h_top) > 1
                c1 = mean(h1V);
            else
                c1 = h1V;
            end
            if length(h_bot) > 1
                c2 = mean(h2V);
            else
                c2 = h2V;
            end
            [~,shapename,~] = fileparts(shapepath);
            
            graycolor = [0.7 0.7 0.7];
            if show_fig
                figure;
                title('twist');
                camlight;
                cameratoolbar('SetMode', 'orbit');
                cameratoolbar('SetCoordSys', 'none');
                axis vis3d;
                hold on;
            end
            for i = 0:n+1
                angle = minA + (maxA-minA)*i/(n+1);
                ray1 = [c1 d'];
                ray2 = [c2 d'];
                rot1 = createRotation3dLineAngle(ray1, deg2rad(angle));
                rot2 = createRotation3dLineAngle(ray2, -deg2rad(angle));
%                 rot1 = rot1(1:3,1:3);
%                 rot2 = rot2(1:3,1:3);
%                 h1V_new = (rot1 * h1V')';
%                 h2V_new = (rot2 * h2V')';
                h1V_new = transformPoint3d(h1V, rot1);
                h2V_new = transformPoint3d(h2V, rot2);
                initV = m.V;
                initV(h_top,:) = h1V_new;
                initV(h_bot,:) = h2V_new;
                
                %export initmesh to file
                if show_fig
                    %plot_mesh(initV,m.B);
                    drawPoint3d([h1V_new;h2V_new]);
                    drawMesh(initV, m.F, 'EdgeColor','none','FaceColor',graycolor,'FaceLighting','flat');
                end
                init_shapename = [shapename '_init_' pad(num2str(i),4,'left','0')];
                filebase = fullfile(folder, init_shapename);
                write_tetgennode(initV, filebase);
                createLink2tetgenFace(filebase, shapepath);
                createLink2tetgenTets(filebase, shapepath);
                
                % export meta info: angle, rot-axis
                meta_file = fullfile(folder, [init_shapename '.testmeta']);
                TestUtil.exportMeta(meta_file, 'twist', h_top, h_bot, [], d, angle);
            end
            
            % export handles
            hdl_file = [fullfile(folder,[shapename '_hdls']) '.dmat'];
            writeDMAT(hdl_file, [h_top;h_bot]-1);
            if show_fig
                hold off;
            end
            
        end
        
        function gen_stretch(folder, shapepath, m, ...
                d, h_top, h_bot, min_s, max_s, n, show_fig)
            %% generating stretching tests
            folder = fullfile(folder, 'stretch');
            % if exist(folder, 'dir')
            %     fprintf('folder "%s" exists; skipping gen test...', folder);
            %     return
            % end
            if exist(folder, 'dir')
                warning('folder "%s" exists; overwriting...', folder);
                rmdir(folder,'s');
            end
            mkdir(folder);
            h1V = m.V(h_top,:);
            h2V = m.V(h_bot,:);
            if length(h_top) > 1
                c1 = mean(h1V);
            else
                c1 = h1V;
            end
            if length(h_bot) > 1
                c2 = mean(h2V);
            else
                c1 = h2V;
            end
            [~,shapename,~] = fileparts(shapepath);
            
            graycolor = [0.7 0.7 0.7];
            if show_fig
                figure;
                title('stretch');
                c = camlight('headlight');
                cameratoolbar('SetMode', 'orbit');
                cameratoolbar('SetCoordSys', 'none');
                axis vis3d;
                hold on;
            end
            for i = 0:n+1
                %stretch handle set top, and fix handle set bot
                s = min_s + (max_s-min_s)*i/(n+1);
                h1V_new = h1V + s*d';
                initV = m.V;
                initV(h_top,:) = h1V_new;
                
                %export init vertices to file
                if show_fig
                    %plot_mesh(initV,m.B);
                    drawPoint3d([h1V_new;h2V]);
                    drawMesh(initV, m.B, 'EdgeColor','none','FaceColor',graycolor);
                end
                init_shapename = [shapename '_init_' pad(num2str(i),4,'left','0')];
                filebase = fullfile(folder, init_shapename);
                write_tetgennode(initV, filebase);
                createLink2tetgenFace(filebase, shapepath);
                createLink2tetgenTets(filebase, shapepath);
                
                % export meta info: strech amount, strech direction
                meta_file = fullfile(folder, [init_shapename '.testmeta']);
                TestUtil.exportMeta(meta_file, 'stretch', h_top, h_bot, [], d, s);
            end
            hdl_file = [fullfile(folder,[shapename '_hdls']) '.dmat'];
            writeDMAT(hdl_file, [h_top;h_bot]-1);
            if show_fig
                hold off;
            end
        end
        
        function gen_bend(folder, shapepath, m, ...
                d, h_top, h_bot, h_mid, minA, maxA, n, show_fig)
            %% generating bending tests
            folder = fullfile(folder, 'bend');
            % if exist(folder, 'dir')
            %     fprintf('folder "%s" exists; skipping gen test...', folder);
            %     return
            % end
            if exist(folder, 'dir')
                warning('folder "%s" exists; overwriting...', folder);
                rmdir(folder,'s');
            end
            mkdir(folder);
            h1V = m.V(h_top,:);
            h2V = m.V(h_bot,:);
            h3V = m.V(h_mid,:);
            %c1 = mean(h1V);
            %c2 = mean(h2V);
            if length(h_mid) > 1
                c3 = mean(h3V);
            else
                c3 = h3V;
            end
            [~,shapename,~] = fileparts(shapepath);
            
            graycolor = [0.7 0.7 0.7];
            if show_fig
                figure;
                title('bend');
                camlight;
                cameratoolbar('SetMode', 'orbit');
                cameratoolbar('SetCoordSys', 'none');
                axis vis3d;
                hold on;
            end
            for i = 0:n+1
                angle = minA + (maxA-minA)*i/(n+1);
                ray1 = [c3 d'];
                ray2 = [c3 d'];
                rot1 = createRotation3dLineAngle(ray1, -deg2rad(angle));
                rot2 = createRotation3dLineAngle(ray2, deg2rad(angle));
%                 rot1 = rot1(1:3,1:3);
%                 rot2 = rot2(1:3,1:3);
%                 h1V_new = (rot1 * h1V')';
%                 h2V_new = (rot2 * h2V')';
                h1V_new = transformPoint3d(h1V, rot1);
                h2V_new = transformPoint3d(h2V, rot2);
                initV = m.V;
                initV(h_top,:) = h1V_new;
                initV(h_bot,:) = h2V_new;
                
                %export to file
                if show_fig && i == floor(n+1)
                    %plot_mesh(initV,m.B);
                    drawPoint3d([h1V_new;h2V_new;h3V]);
                    drawMesh(initV, m.F, 'EdgeColor','none','FaceColor',graycolor);
                end
                init_shapename = [shapename '_init_' pad(num2str(i),4,'left','0')];
                filebase = fullfile(folder, init_shapename);
                write_tetgennode(initV, filebase);
                createLink2tetgenFace(filebase, shapepath);
                createLink2tetgenTets(filebase, shapepath);
                
                % export meta info: bend angle, bend direction
                meta_file = fullfile(folder, [init_shapename '.testmeta']);
                TestUtil.exportMeta(meta_file, 'bend', h_top, h_bot, h_mid, d, angle);
            end
            hdl_file = [fullfile(folder,[shapename '_hdls']) '.dmat'];
            writeDMAT(hdl_file, [h_top;h_bot;h_mid]-1);
            if show_fig
                hold off;
            end
        end
    
        function exportMeta(meta_file, tp, h_top, h_bot, h_mid, d, amnt)
            %% export meta info to reproduce this test or deform-path:
            % def amount, def direction
            meta_fid = fopen(meta_file, 'wt');
            fprintf(meta_fid, '%s\n', tp);
            fprintf(meta_fid, '%d %d %d\n', numel(h_top), numel(h_bot), numel(h_mid));
            fprintf(meta_fid, '%f %f %f\n', d);
            fprintf(meta_fid, '%f\n', amnt);
            fclose(meta_fid);
        end
        
        function [dfm_tp, n_top, n_bot, n_mid, d, amnt] = importMeta(meta_file)
            %% read meta info from a meta file
            meta_fid = fopen(meta_file, 'rt');
            % type of deform
            dfm_tp = strtrim(fgetl(meta_fid));
            % hdl num in groups
            ln = strtrim(fgetl(meta_fid));
            hdln_in_group = sscanf(ln, '%d %d %d');
            n_top = hdln_in_group(1);
            n_bot = hdln_in_group(2);
            n_mid = hdln_in_group(3);
            % deform axis
            ln = strtrim(fgetl(meta_fid));
            d = sscanf(ln, '%f %f %f');
            % deform amount
            ln = strtrim(fgetl(meta_fid));
            amnt = sscanf(ln, '%f');
            fclose(meta_fid);
        end
        
        function dfm_info = mkDeformInfo(meta_file, hdls)
            %% wrap info from meta file into a DeformInfo struct
            [tp,n_top,n_bot,n_mid,d,amnt] = TestUtil.importMeta(meta_file);
            dfm_info.tp = tp;
            dfm_info.h_top = hdls(1:n_top);
            dfm_info.h_bot = hdls(n_top+1:n_top+n_bot);
            dfm_info.h_mid = hdls(n_top+n_bot+1:end);
            dfm_info.d = d;
            dfm_info.min = 0.0;
            dfm_info.max = amnt;
        end
        
        function V = sampleDeformPath(dfm_info, m, alpha, show_fig)
            %% interpolate a specified deform path
            if strcmp(dfm_info.tp,'twist')
                V = TestUtil.sampleTwistPath(m, dfm_info.d, ...
                    dfm_info.h_top, dfm_info.h_bot, ...
                    dfm_info.min, dfm_info.max, alpha, show_fig);
            elseif strcmp(dfm_info.tp,'stretch')
                V = TestUtil.sampleStretchPath(m,dfm_info.d, ...
                    dfm_info.h_top, dfm_info.h_bot, ...
                    dfm_info.min, dfm_info.max, alpha,show_fig);
            elseif strcmp(dfm_info.tp,'bend')
                V = TestUtil.sampleBendPath(m,dfm_info.d, ...
                    dfm_info.h_top, dfm_info.h_bot, dfm_info.h_mid, ...
                    dfm_info.min, dfm_info.max, alpha, show_fig);
            else
                error(['Unsupported deform type: ' dfm_info.tp]);
            end
        end
        
        function initV = sampleTwistPath(m, d, h_top, h_bot, minA, maxA, ...
                alpha, show_fig)
            %% interpolate a twist path between two angles
            h1V = m.V(h_top,:);
            h2V = m.V(h_bot,:);
            if length(h_top) > 1
                c1 = mean(h1V);
            else
                c1 = h1V;
            end
            if length(h_bot) > 1
                c2 = mean(h2V);
            else
                c2 = h2V;
            end
            
            graycolor = [0.7 0.7 0.7];
            if show_fig
                figure;
                title('twist');
                camlight;
                cameratoolbar('SetMode', 'orbit');
                cameratoolbar('SetCoordSys', 'none');
                axis vis3d;
                hold on;
            end
            
            angle = minA + (maxA-minA)*alpha;
            ray1 = [c1 d'];
            ray2 = [c2 d'];
            rot1 = createRotation3dLineAngle(ray1, deg2rad(angle));
            rot2 = createRotation3dLineAngle(ray2, -deg2rad(angle));
%             rot1 = rot1(1:3,1:3);
%             rot2 = rot2(1:3,1:3);
%             h1V_new = (rot1 * h1V')';
%             h2V_new = (rot2 * h2V')';
            h1V_new = transformPoint3d(h1V, rot1);
            h2V_new = transformPoint3d(h2V, rot2);
            initV = m.V;
            initV(h_top,:) = h1V_new;
            initV(h_bot,:) = h2V_new;
            
            if show_fig
                %plot_mesh(initV,m.B);
                drawPoint3d([h1V_new;h2V_new]);
                drawMesh(initV, m.F, 'EdgeColor','none','FaceColor',graycolor,'FaceLighting','flat');
                hold off;
            end
        end
        
        function initV = sampleStretchPath(m, d, h_top, h_bot, min_s, max_s,...
                alpha, show_fig)
            %% interpolate a stretch path between min/max stretch amounts
            h1V = m.V(h_top,:);
            h2V = m.V(h_bot,:);
            %             if length(h_top) > 1
            %                 c1 = mean(h1V);
            %             else
            %                 c1 = h1V;
            %             end
            %             if length(h_bot) > 1
            %                 c2 = mean(h2V);
            %             else
            %                 c1 = h2V;
            %             end
            
            graycolor = [0.7 0.7 0.7];
            if show_fig
                figure;
                title('stretch');
                c = camlight('headlight');
                cameratoolbar('SetMode', 'orbit');
                cameratoolbar('SetCoordSys', 'none');
                axis vis3d;
                hold on;
            end
            %stretch handle set top, and fix handle set bot
            s = min_s + (max_s-min_s)*alpha;
            h1V_new = h1V + s*d';
            initV = m.V;
            initV(h_top,:) = h1V_new;
            
            %export init vertices to file
            if show_fig
                %plot_mesh(initV,m.B);
                drawPoint3d([h1V_new;h2V]);
                drawMesh(initV, m.B, 'EdgeColor','none','FaceColor',graycolor);
            end
            if show_fig
                hold off;
            end
        end
        
        function initV = sampleBendPath(m, d, h_top, h_bot, h_mid, minA, maxA, ...
                alpha, show_fig)
            %% interpolate a bending deform path
            h1V = m.V(h_top,:);
            h2V = m.V(h_bot,:);
            h3V = m.V(h_mid,:);
            %c1 = mean(h1V);
            %c2 = mean(h2V);
            if length(h_mid) > 1
                c3 = mean(h3V);
            else
                c3 = h3V;
            end
            
            graycolor = [0.7 0.7 0.7];
            if show_fig
                figure;
                title('bend');
                camlight;
                cameratoolbar('SetMode', 'orbit');
                cameratoolbar('SetCoordSys', 'none');
                axis vis3d;
                hold on;
            end
            angle = minA + (maxA-minA)*alpha;
            ray1 = [c3 d'];
            ray2 = [c3 d'];
            rot1 = createRotation3dLineAngle(ray1, -deg2rad(angle));
            rot2 = createRotation3dLineAngle(ray2, deg2rad(angle));
%             rot1 = rot1(1:3,1:3);
%             rot2 = rot2(1:3,1:3);
%             h1V_new = (rot1 * h1V')';
%             h2V_new = (rot2 * h2V')';
            h1V_new = transformPoint3d(h1V, rot1);
            h2V_new = transformPoint3d(h2V, rot2);
            initV = m.V;
            initV(h_top,:) = h1V_new;
            initV(h_bot,:) = h2V_new;
            
            if show_fig % && smpl_i == floor(n_smpls+1)
                %plot_mesh(initV,m.B);
                drawPoint3d([h1V_new;h2V_new;h3V]);
                drawMesh(initV, m.F, 'EdgeColor','none','FaceColor',graycolor);
                hold off;
            end
        end
    end
    
end

