% scan the folder to locate all test-able shapes
function makeTestRunFile( format, allshapefolder, testrunfile )
    % only grab subfolders' names (w/o . or ..)
    dirinfo = dir(allshapefolder);
    isub = [dirinfo(:).isdir];
    foldnames = {dirinfo(isub).name}';
    % remove . and ..
    foldnames(ismember(foldnames, {'.','..'})) = [];
    foldnamestr = strjoin(foldnames,'\n');
    
    % make a test-gen file
    fid = fopen(testrunfile, 'wt');
    max_iter = '100';
    Ks = '1 0.5 3';
    content = strjoin({...
        format,...
        '3d',...
        'twist stretch bend',...
        max_iter,...
        Ks,...
        foldnamestr...
        },'\n');
    fprintf(fid, '%s', content);
    fclose(fid);
end