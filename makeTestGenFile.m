% scan the folder to locate all test-able shapes
function makeTestGenFile( format, allshapefolder, testgenfile )
    % only grab subfolders' names (w/o . or ..)
    dirinfo = dir(allshapefolder);
    isub = [dirinfo(:).isdir];
    foldnames = {dirinfo(isub).name}';
    % remove . and ..
    foldnames(ismember(foldnames, {'.','..'})) = [];
    foldnamestr = strjoin(foldnames,'\n');
    
    % make a test-gen file
    fid = fopen(testgenfile, 'wt');
    content = strjoin({...
        format,...
        '3d',...
        'twist stretch bend',...
        '0.15 0.15 0.15',...
        foldnamestr,...
        },'\n');
    fprintf(fid, '%s', content);
    fclose(fid);
end