%% plot evolution of (min/max)SVs across all iteraions for simplex i
function plotSVs(alliter_min, alliter_max, i, option)
    t_minsv = alliter_min(i,:);
    t_maxsv = alliter_max(i,:);
    plot(t_minsv, t_maxsv, option);
    label_perPoint = cellstr(num2str( (1:numel(t_maxsv))' ));
    text(t_minsv, t_maxsv, label_perPoint);
    axis on;
    xlabel('min SV');
    ylabel('max SV');
end