%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wrote my own mse (otherwise a neural network toolkit license is
% required...)
% Given a vector(supposedly representing difference between two vectors),
% return the mean-squared error
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function err = mse(v_diff)
    err = sum( v_diff.^2 ) / numel( v_diff );
end