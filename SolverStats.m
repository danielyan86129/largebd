classdef SolverStats % inherit handle, i.e. < handle to make this a ref.able object
    %SOLVERSTAT A solver contains this object to store stats & states
    %   Detailed explanation goes here
    
    properties
        final_dist = -1;
        converged = false;
        tol_reached = false;
        inj_iter = -1;
        bd_iter = -1;
        total_iter = -1;
        n_infeasible = -1;
        n_flip = -1;
        % times
        total_t = 0.0;
        iter_t = 0.0;
    end
    
    methods
    end
    
end

