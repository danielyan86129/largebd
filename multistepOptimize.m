function [resultM, suc, stats] = multistepOptimize( restM, initM, hdls, dfm_info, ...
    r_improve, ada_step,...
    K, autoK, max_iter, tol_err,...
    n, show_fig, stop_at_inj, shape_basename, img_folder )
%multistepOptimize Subdivide path between rest and target handle positions
%into multiple steps, and optimize each step
%Input:
%     restM, initM, and hdls
%     def_info: contains info about the deform path, e.g. type
%     (twist/stretch/bend), handles of the mesh involved, etc.
%     r_improve: the ratio to satisfy for a successful step
%     ada_step: use adaptive step or not
%     K the bound
%     autoK: automatically derive K at each step
%     max_iter the max num of iterations per optimization
%     show_fig: show each result as a separate figure?
%     shape_basename: the base name string for rest & init mesh
%     img_folder: the folder to write result images to
%Output:
%     result mesh, success flag, stats of the last success solve.
%% initialization
bbox = boundingBox3d(restM.V);
mesh_sz = max([bbox(2)-bbox(1),bbox(4)-bbox(3),bbox(6)-bbox(5)]);
hdl_tgt = initM.V(hdls,:);
hdl_rest = restM.V(hdls,:);
prevM = restM;
resultM = restM;
n_suc = 0;
graycolor = [0.5,0.5,0.5];
% init solver & params
nextM = prevM;
s = 1 / n;
cur_prog = 0; % track progress (0~1)
nextM.V = TestUtil.sampleDeformPath(dfm_info, restM, s, false);
if autoK
    K = maxDistortion(restM,nextM);
    fprintf('auto K = %f\n',K);
end
[constrLHS, constrRHS] = genConstraintA(hdls, nextM.V, ...
    size(restM.V,2), size(restM.V,1));
solver_bd = SolverProjectorBD(prevM.F, prevM.V, constrLHS, constrRHS, ...
    K, -1, -1, nextM.V, SolverProjectorModeEnum.Tangent, false);
solver_bd.verbose = 0;
step_i = 0; % how many steps so far
suc = true;
min_s = 0.5*1/n; % minimum allowed step
%% keep going if last step succeeded and targets not reached and have budget
while (suc && ...
        norm(max(abs(resultM.V(hdls,:)-hdl_tgt))) > mesh_sz*1e-3 && ...
        abs(1 - cur_prog) > 1e-7 && ...
        step_i < n)
    step_i = step_i + 1;
    %% optimize for this step
    % save cur solver states
    solver_bd.save();
    trial_i = 0; % how many times tried in one step
    if ada_step && s < min_s % reboot step size if too small
        s = min(1/n, 1-cur_prog);
    end
    while s >= min_s % may need to solve more than once
        trial_i = trial_i + 1;
        new_prog = cur_prog + s;
        nextM.V = TestUtil.sampleDeformPath(dfm_info, restM, new_prog, false);
        % update the linear sub-space & project cur X to that space
        % by calling solve with max-iter == 1
        solver_bd.setHandlePos(nextM.V(hdls,:));
        solver_bd.solve(1, tol_err, '', false, false, false, stop_at_inj);
        % save the distortion at beginning of the step
        step_init_d = solver_bd.distortions;
        fprintf('init dist of cur step: %f\n',max(step_init_d));
        % only solve when solver not converged
        if ~solver_bd.stats.converged && max_iter > 1
            solver_bd.solve(max_iter, tol_err, '', false, false, false, stop_at_inj);
        end
        %% extract result
        stats = solver_bd.stats;
        resultM.V = solver_bd.y;
        resultM.F = solver_bd.F;
        resultM.B = boundary_faces(resultM.F);
        if stop_at_inj
            suc = stats.n_flip==0;
        elseif abs(new_prog - 1) > eps(1) % not final step
            % accept as long as step improves
            if max(step_init_d)-K < 0.00001
                r = 1;
            else
                r = (max(step_init_d) - max(solver_bd.distortions)) / (max(step_init_d)-K);
            end
            suc = stats.n_flip==0 && (stats.tol_reached || r > r_improve);
        else % reaching final goal...
            suc = stats.n_flip == 0 && stats.tol_reached;
        end
        stats.final_dist = max(solver_bd.distortions);
        
        %% analyze & tally result
        fprintf('step %d (trial %d, progress %f), step len %f, iters %d, time %f. ',...
            step_i,trial_i,cur_prog,s,solver_bd.stats.total_iter,solver_bd.t_iter_total);
        if suc % get ready to move to next step
            fprintf('success. \n');
            n_suc = n_suc + 1;
            cur_prog = new_prog;
            if ada_step
                s = min(s*2, 1-cur_prog);
            end
            % show figure for result if necessary
            fg = -1;
            if show_fig
                fg = figure;
            else
                fg = figure('visible','off');
            end
            hold on;
            cameratoolbar('SetMode', 'orbit');
            cameratoolbar('SetCoordSys', 'none');
            axis equal;
            axis vis3d;
            drawMesh(resultM.V,resultM.F,'FaceColor',graycolor,'FaceAlpha',0.1,'EdgeAlpha',0.1);
            if length(resultM.V(1,:)) == 3
                drawpoint = @drawPoint3d;
            else
                drawpoint = @drawPoint;
            end
            drawpoint(resultM.V(hdls,:),'Marker','o','MarkerFaceColor','r','MarkerEdgeColor','none');
            drawpoint(hdl_tgt,'Marker','o','MarkerFaceColor','g','MarkerEdgeColor','none');
            hold off;
            % save figure for this step
            fg_path = fullfile(img_folder, [shape_basename '-' num2str(K) '-' pad(num2str(step_i),4,'left','0') '.png']);
            saveas(fg, fg_path);
            close(fg);
            
            break;
        else % resolve | quit
            fprintf('failure. \n');
            if ada_step % shrink step & resolve
                s = s / 2;
                solver_bd.restore();
            else
                fprintf('Failed at progress %f.\n', cur_prog);
                return;
            end
        end
    end
end
% % solve failed if final goal is not reached
% suc = norm(max(abs(resultM.V(hdls,:)-hdl_tgt))) <= mesh_sz*0.000001;
end

