
function m = importMesh( basename, d, format )
%IMPORTMESH reads in a mesh of dimension d
%   returns a structure of tuple <V, F>
if d == '2d'
    if format == 'obj'
        [m.V, m.F] = readOBJ( [char(basename) '.' format] );
    else 
        error(['Format unsupported! ' format]);
    end
    m.V(:, 3) = []; % only keep 2 coordinates
elseif strcmp(d,'3d')
    if strcmp(format,'dmat')
        m.V = readDMAT( [basename '.node.dmat'] );
        m.F = readDMAT( [basename '.elem.dmat'] );
    elseif strcmp(format,'tetgen')
        [m.V, m.B, m.F] = read_tetgenmesh( basename );
        m.V = m.V(:,1:3); % make sure only 3d points
        m.B = m.B + 1; % make index 1-based
    else
        error(['Unsupported format: ',format]);
    end
    m.F = m.F + 1; % make index 1-based
else
    error('invalid dimension specified!');
end

end

