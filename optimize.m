function [ result_m ] = optimize( dim, basename, K, iter_max )
%OPTIMIZE Find the injective mapping of the problem indicated by the
%basename
%   The rest & init meshes and the handle indices are read from files: 
%   basename.obj, basename_init.obj, and basename_hdls.dmat

if dim == 2
    [rest_m, init_m, hdls] = import2D( basename );
else
    [rest_m, init_m, hdls] = import3D( basename );
end

lb = -1; % lower bound on SVs (-1 = disabled)
ub = -1; % upper bound on SVs (-1 = disabled)
n = size(rest_m.V, 1);
d = size(rest_m.V, 2);
tol_err = 1e-50; % BD projection tolerance
use_weighted_metric = false; % use a weighted metric?

x0 = init_m.V;
[C_lh, C_rh] = genConstraintA( hdls, x0, d, n );

solver_bd = SolverProjectorBD( rest_m.F, rest_m.V, C_lh, C_rh, K, lb, ub, x0, ...
    SolverProjectorModeEnum.Tangent, use_weighted_metric );

% axis equal
% axis vis3d

% plot rest shape
visualize( rest_m.V, rest_m.F, K );
title('Rest shape');
hF(1) = gcf;
hA(1) = gca;

% plot initial map
figure;
solver_bd.visualize();
title('Initial Map');
cameratoolbar;
cameratoolbar('SetCoordSys','none');
% hF(2) = gcf;
hA(2) = gca;

% run solver
record_fig = false;
t_solve = tic;
fig_res = solver_bd.solve( iter_max, tol_err, record_fig ); % solve BD projection
t_solve = tic(t_solve);
fprintf('Time elapsed: solve - %f, iterate - %f\n',t_solve,solve_bd.t_iter_total);
% post-process figures
if record_fig
    video_base = fileparts( mfilename('fullpath') );
    video_base = fullfile( video_base, 'videos', '2d' );
    saveFigs(fig_res, video_base);
end

% set results
result_m.V = solver_bd.y;
result_m.F = solver_bd.F;

% plot output map
figure;
solver_bd.visualize();
title('Output Map');
cameratoolbar;
cameratoolbar('SetCoordSys','none');
% hF(2) = gcf;
hA(3) = gca;

% linkaxes(hA);
% for i=1:numel(hF)
%     camproj(hA(i), 'perspective');
%     cameratoolbar(hF(i), 'SetMode', 'orbit');
%     cameratoolbar(hF(i), 'SetCoordSys', 'none');
%     cameratoolbar(hF(i), 'Show');
% end

% plot SVs evolution
% figure;
% plotSVs(solver_bd.alliter_minsv, solver_bd.alliter_maxsv, 1, 'b--x');
% figure;
% plotSVs(solver_bd.alliter_minsv, solver_bd.alliter_maxsv, 2, 'r--o');

end